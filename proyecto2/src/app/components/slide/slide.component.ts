import { Component, Input, OnInit } from '@angular/core';
import { ISlide } from '../../models/slide.interface';
import { timer } from 'rxjs';


@Component({
  selector: 'app-slide',
  templateUrl: './slide.component.html',
  styleUrls: ['./slide.component.scss']
})
export class SlideComponent implements OnInit {
  // propiedades del slide
  @Input() height = 500;
  @Input() fullScreen = false;
  @Input() items: ISlide[] = [];

  public finalHeight: string | number = 0;
  public currentPosition = 0;


  constructor() {
    // establecer el tamaño del slide
    this.finalHeight = this.fullScreen ? '100vh' : `${this.height}px`;
  }

  ngOnInit(): void {
    // recorrer el array de items (el contenido del slide) y asignarle un id y 
    // un margen para facilitar el manejo de este
    this.items.map((i, index) => {
      i.id = index;
      i.marginLeft = 0;
    });
    // recorrer el slide cada 5 segundos
    const source = timer(5000, 5000);
    const subscribe = source.subscribe(val => this.setNext());
  }

  // establecer la posicion de la imagen que se va a visualizar, recorriendo el margen -100
  setCurrentPosition(position: number) {
    this.currentPosition = position;
    this.items.find(i => i.id === 0).marginLeft = -100 * position;
  }

// mover a la siguiente imagen
  setNext() {
    let finalPercentage = 0;
    let nextPosition = this.currentPosition + 1;
    if (nextPosition <= this.items.length - 1) {
      finalPercentage = -100 * nextPosition;
    } else {
      nextPosition = 0;
    }
    this.items.find(i => i.id === 0).marginLeft = finalPercentage;
    this.currentPosition = nextPosition;
  }

  //mover a la anterior imagen
  setBack() {
    let finalPercentage = 0;
    let backPosition = this.currentPosition - 1;
    if (backPosition >= 0) {
      finalPercentage = -100 * backPosition;
    } else {
      backPosition = this.items.length - 1;
      finalPercentage = -100 * backPosition;

    }
    this.items.find(i => i.id === 0).marginLeft = finalPercentage;
    this.currentPosition = backPosition;
  }
}
