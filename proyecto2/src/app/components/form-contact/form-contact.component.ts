import { UpperCasePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IdatosContacto } from '../../models/formContacto.interface';

@Component({
  selector: 'app-form-contact',
  templateUrl: './form-contact.component.html',
  styleUrls: ['./form-contact.component.scss']
})
export class FormContactComponent implements OnInit {
  formularioContacto: FormGroup;
  formDataContacto: IdatosContacto;


  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.crearFormulario();
  }
  // funcion para guarda los datos en local storage
  onSubmitContacto() {
    console.log('formulario datos', this.formularioContacto.value);
    this.formDataContacto = this.formularioContacto.value;
    localStorage.setItem('datosContacto', JSON.stringify(this.formDataContacto));
    this.formularioContacto.setValue({ name: '' , correo: '', mensaje: '' });
  }
  //funcion para crear del formulario reactivo
  crearFormulario() {
    this.formularioContacto = this.fb.group({
      name: [''],
      correo: [''],
      mensaje: ['']
    });
  }
}
