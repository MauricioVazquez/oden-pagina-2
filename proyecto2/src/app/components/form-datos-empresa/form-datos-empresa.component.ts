import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { IdatosEmpresa } from '../../models/formEmpresa.interface';

@Component({
  selector: 'app-form-datos-empresa',
  templateUrl: './form-datos-empresa.component.html',
  styleUrls: ['./form-datos-empresa.component.scss']
})
export class FormDatosEmpresaComponent implements OnInit {
  formularioDatos: FormGroup;
  formDataEmpresa: IdatosEmpresa;
  name = '';
  content = '';
  checked = false;
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    // se crea el form 
    this.crearFormulario();
    // si no hay datos guardados en local storage crea 3 por defecto, si no los recupera
    if (localStorage.getItem('datosEmpresa') === null) {
      this.anadirDatoEmpresa('Correo', 'empresa@empresa.edu.mx');
      this.anadirDatoEmpresa('Dirección', 'Av.24 Real providencia #486');
      this.anadirDatoEmpresa('Telefono', '055 3545 69');
    } else {
      const getDatos: IdatosEmpresa = JSON.parse(localStorage.getItem('datosEmpresa'));
      for (let item of Object.keys(getDatos)) {
        console.log(getDatos[item]);
        for (let item2 of getDatos[item]) {
          this.anadirDatoEmpresa(item2.name, item2.content);
        }
      }
    }
  }
  // funcion para cambiar la directiva if y saber si es admin o no
  onChange(e) {
    this.checked = !this.checked;
  }
  // funcion para guardar los datos en local storage
  onSubmitEmpresa() {
    console.log('formulario datos', this.formularioDatos.value);
    this.formDataEmpresa = this.formularioDatos.value;
    localStorage.setItem('datosEmpresa', JSON.stringify(this.formDataEmpresa));
  }
  // funcion para crear el formulario reactivo
  crearFormulario() {
    this.formularioDatos = this.fb.group({
      datosEmpresa: this.fb.array([])
    });
  }
  // funcion para obtener el formulario
  get datosEmpresa(): FormArray {
    return this.formularioDatos.get('datosEmpresa') as FormArray;
  }

  // funcion para añadir un nuevo item al array del formulario
  anadirDatoEmpresa(name, content) {
    const datos = this.fb.group({
      name: [name],
      content: [content],
    });
    this.datosEmpresa.push(datos);
    this.name = '';
    this.content = '';
  }
  // funcion para borrar un item del formularion buscando el id
  borrarDatoEmpresa(indice: number) {
    this.datosEmpresa.removeAt(indice);
  }
}
