import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { MaterialModule } from '../material/material.module';
import { SlideComponent } from './slide/slide.component';
import { FormContactComponent } from './form-contact/form-contact.component';
import { FormDatosEmpresaComponent } from './form-datos-empresa/form-datos-empresa.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  // modulo components agrupa todos los componentes, con el fin de organizar el codigo
  declarations: [HeaderComponent, SlideComponent, FormContactComponent, FormDatosEmpresaComponent],
  exports: [HeaderComponent, SlideComponent, FormContactComponent, FormDatosEmpresaComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
  ]
})
export class ComponentsModule { }
