import { Component, OnInit } from '@angular/core';
import { ISlide } from '../models/slide.interface';
import { SILEDATA } from '../models/slideData';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  // datos del slide
  public slideData: ISlide[] = SILEDATA;

  constructor() { }

  ngOnInit(): void {

  }

}
