export interface IdatosContacto {
    name: string;
    correo: string;
    mensaje: string;
}