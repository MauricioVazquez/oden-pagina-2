export interface ISlide {
    id: number;
    tittle?: string;
    subtittle?: string;
    link?: string;
    img: string;
    order?: string;
    marginLeft?: number;
}
